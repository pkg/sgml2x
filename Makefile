DESTDIR =
prefix  = /usr/local
DOCDIR  = ${prefix}/share/doc/sgml2x
SGMLDIR = ${prefix}/share/sgml
MANDIR  = ${prefix}/share/man
BINDIR  = ${prefix}/bin
CONFDIR = ${prefix}/etc

VERSION = 1.0.0
CVSTAG = RELEASE_$(subst .,-,${VERSION})

TARGETFORMATS = ps pdf fot html rtf mif
export CONVERTERS = $(patsubst %,docbook-2-%,${TARGETFORMATS})

CONVERTERFILES = $(addprefix bin/,${CONVERTERS})

DB2XOPTS = -c catalog -C ./sgml2x-aliases

all: s2x catalog-installable docs

s2x: ${CONVERTERFILES}

docs: TODO-html TODO.txt
	cd doc && ${MAKE} all

ChangeLog:
	cvs2cl --revisions

catalog-installable: catalog-installable.in
	sed 's|@SGMLDIR@|${SGMLDIR}|g' < $< > $@

install: all
	install -d -m755 ${DESTDIR}${BINDIR}
  # catalog
	install -d -m755 ${DESTDIR}${SGMLDIR}/misc
	install -m644 catalog-installable ${DESTDIR}${SGMLDIR}/misc/sgml2x.catalog
  # dsssl
	set -e && for dtd in docbook; do for dir in misc; do \
	  if test -d stylesheet/dsssl/$${dtd}/$${dir}; then \
	    install -d -m755 ${DESTDIR}${SGMLDIR}/$${dtd}/stylesheet/dsssl/alcove/$${dir} ;\
	    install -m644 stylesheet/dsssl//$${dtd}/$${dir}/*.* \
		${DESTDIR}${SGMLDIR}/$${dtd}/stylesheet/dsssl/alcove/$${dir} ;\
	  fi ;\
	done; done
  # sgml2x scripts
	cd ${DESTDIR}${BINDIR}/ && rm -f ${CONVERTERS}
	cp -dp ${CONVERTERFILES} ${DESTDIR}${BINDIR}/
	sed s/@VERSION@/${VERSION}/ bin/sgml2x >${DESTDIR}${BINDIR}/sgml2x
	chmod 755 ${DESTDIR}${BINDIR}/sgml2x
	touch -r bin/sgml2x ${DESTDIR}${BINDIR}/sgml2x

  # sgml2x conf
	install -d -m755 ${DESTDIR}${CONFDIR}/sgml/sgml2x
	cp -a sgml2x-aliases ${DESTDIR}${CONFDIR}/sgml/sgml2x/styles
	cd ${DESTDIR}${CONFDIR}/sgml/sgml2x/ && mv styles/dssslproc .
	find ${DESTDIR}${CONFDIR}/sgml/sgml2x -type d -name CVS | xargs rm -rf

  # support files
	install -d -m755 ${DESTDIR}${SGMLDIR}/sgml2x/
	cp lib/jadetex.cfg ${DESTDIR}${SGMLDIR}/sgml2x/

  # misc scripts
	install -m755 bin/rlatex bin/runjade ${DESTDIR}${BINDIR}/

  # docs
	install -d -m755 ${DESTDIR}${DOCDIR}
	cp -p TODO.txt ${DESTDIR}${DOCDIR}/TODO

	cd doc && ${MAKE} install

checkversion:
	test `head -1 debian/changelog | sed 's/^.*(\(.*\)-.*$$/\1/'` = ${VERSION}

distdir: checkversion
	rm -rf sgml2x-${VERSION}
	cvs -q export -r ${CVSTAG} -d sgml2x-${VERSION} sgml2x

dist: distdir
	tar zcf sgml2x-${VERSION}.tar.gz sgml2x-${VERSION}
	rm -rf sgml2x-${VERSION}

upload: dist
	scp sgml2x-${VERSION}.tar.gz ydirson@freesoftware.fsf.org:/upload/alcovebook/

${CONVERTERFILES}: bin/docbook-2-%:
	rm -f $@
	ln -s sgml2x $@

clean::
	rm -f ${CONVERTERFILES} TODO.txt catalog-installable
	cd doc && ${MAKE} clean

# Misc doc-building rules

%.ps: %.sgml bin/docbook-2-ps
	bin/docbook-2-ps ${DB2XOPTS} $<

%.pdf: %.sgml bin/docbook-2-pdf
	bin/docbook-2-pdf ${DB2XOPTS} $<

%-html: %.sgml bin/docbook-2-html
	bin/docbook-2-html ${DB2XOPTS} -s nwalsh $<

%.txt: %-html
	elinks -no-home -dump $</t1.htm >$@

clean::
	rm -f *.ps *.pdf *.fot *~
	rm -rf *-html

.PHONY: ChangeLog clean all s2x docs install checkversion dist distdir upload
