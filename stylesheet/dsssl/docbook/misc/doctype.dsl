<!--
	Stylesheet to extract the "class" or "role" attribute
	from a DocBook article.

	Please refer to this stylesheet using the following public ID:

		-//Alcove//DOCUMENT DocBook DocumentClass Stylesheet//EN
-->

<!DOCTYPE style-sheet PUBLIC "-//OpenJade//DTD DSSSL Specification//EN">
<style-sheet>
  <features>online</features>
  <features>expression</features>
  <features>query</features>

  <style-specification id="print">
    <style-specification-body>

(root
    (let* ((root-element (node-property 'document-element
					(current-node) ))
	   (role (or (attribute-string (general-name-normalize "class"
							       root-element)
				       root-element)
		     (attribute-string (general-name-normalize "role"
							       root-element)
				       root-element) )))
      (make scroll
	(make paragraph
	  (literal (if role
		       role
		       ""))))))

    </style-specification-body>
  </style-specification>
</style-sheet>
